# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import os

import webapp2
from google.appengine.ext import ndb
from webapp2_extras import json

from app.utils import Util

PRODUCTION_MODE = not os.environ.get('SERVER_SOFTWARE', 'Development').startswith('Development')
CURRENT_SERVER = os.environ.get('SERVER_PRODUCTION') if PRODUCTION_MODE else os.environ.get('SERVER_DEVELOPMENT')


class ApiRequestHandler(webapp2.RequestHandler):
    """ handler for Travel API requests """

    log = logging  # no need to import logging everywhere
    json = json  # no need to import webapp2_extras.json everywhere
    ndb = ndb  # no need to import ndb everywhere
    util = Util  # no need to import the utils everywhere

    iata = None
    origin = None
    destination = None
    memcache_key = None

    def initialize(self, request, response):
        super(ApiRequestHandler, self).initialize(request, response)

        kwargs = self.request.route_kwargs

        keys = {
            'iata': kwargs.get('iata'),
            'origin': kwargs.get('origin'),
            'destination': kwargs.get('destination')
        }  # IATA kwarg keys to watch for

        airports = {v.strip(): k for k, v in keys.iteritems() if v}

        for airport in airports.keys():
            if len(airport) != 3:
                pass  # return self.render_fail("Invalid airport code passed")

        airport_keys = airports.keys()
        for i, airport in enumerate(ndb.get_multi([ndb.Key('IATA', airport) for airport in airport_keys])):

            if not airport:
                continue  # return self.render_fail("Airport {} not found".format(airport_keys[i]))

            setattr(ApiRequestHandler, airports[airport.key.id()], airport)

        self.memcache_key = self.request.path_qs

    def options(self, *args, **kwargs):
        """ OPTIONS is used by browsers for preflighted requests for security reasons.
            See https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#Preflighted_requests """

        self.response.headers.add_header(b"Access-Control-Allow-Origin", CURRENT_SERVER)
        self.response.headers.add_header(b"Access-Control-Allow-Methods", b"GET, POST, PUT, PATCH, DELETE")
        self.response.headers.add_header(b"Access-Control-Allow-Headers",
                                         b"Origin, X-Requested-With, Content-Type, Accept, X-Auth-User, X-Auth-Token")

    def render_api_json(self, obj=None, status='success', message=None, status_code=None):
        """ rendering a response to an API call """

        obj = obj or {}

        self.response.content_type = b'application/json'
        self.response.headers.add_header(b"Access-Control-Allow-Origin", CURRENT_SERVER)
        self.response.headers.add_header(b"Access-Control-Allow-Credentials", b'true')

        # does the client want to cache this response?
        if self.request.get('cache') == 'true':
            # ok, we can make our response cached but by the browser only, no mid-server proxies, etc
            self.response.cache_control = 'private'
            # expire in 300 seconds by default OR whatever the user wants
            self.response.cache_control.max_age = int(self.request.get('cache_max_age', default_value=300))

        if status_code:
            self.response.set_status(status_code)

        obj = {
            'status': status,
            'message': message,
            'data': self.util.to_json(obj)
        }

        encoded_obj = json.encode(obj)

        return self.response.write(encoded_obj)

    def render_fail(self, message='Failed', obj=None, status_code=403):
        """ pre-set helper to render a failed response """

        return self.render_api_json(obj or {}, status='fail', message=message, status_code=status_code)

    def render_success(self, obj=None, message='Success', status_code=200):
        """ pre-set helper to render a success response """

        return self.render_api_json(obj or {}, status='success', message=message, status_code=status_code)

    @webapp2.cached_property
    def model_key(self):
        """ getting the ProductModel key entity via provided model_id parameters """

        model_id = self.request.route_kwargs.get('model_id') if self.request.route_kwargs else None

        return ndb.Key('ProductModel', model_id) if model_id else None

    @webapp2.cached_property
    def model(self):
        """ getting the ProductModel entity via provided model_id parameters """

        return self.model_key.get() if self.model_key else None

    @webapp2.cached_property
    def item_key(self):
        """ getting the ProductItem key entity via provided item_id parameters """

        item_id = self.request.route_kwargs.get('item_id') if self.request.route_kwargs else None

        if item_id and item_id.isdigit():
            return ndb.Key('ProductItem', int(item_id))

        return None

    @webapp2.cached_property
    def model(self):
        """ getting the ProductModel entity via provided model_id parameters """

        return self.model_key.get() if self.model_key else None
