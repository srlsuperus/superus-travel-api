# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from google.appengine.api import memcache

from app.api.handler import ApiRequestHandler
from app.models.Travel import Route, Flight


class Destinations(ApiRequestHandler):
    def get(self):
        routes = memcache.get(self.memcache_key)
        if not routes:
            routes = Route.query(Route.watch == True).fetch(keys_only=True)
            routes = map(lambda x: (x.parent().id(), x.parent().parent().id()), routes)
            memcache.set(self.memcache_key, routes)
        return self.render_success(routes)


class Flights(ApiRequestHandler):
    def get(self, origin, destination):
        flights = memcache.get(self.memcache_key)
        if not flights:
            flights = Flight.query(Flight.origin == origin, Flight.destination == destination).fetch(keys_only=True)
            flights = map(lambda x: x.id(), flights)
            memcache.set(self.memcache_key, flights)
        return self.render_success(flights)
