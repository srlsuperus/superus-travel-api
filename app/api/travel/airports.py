# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from google.appengine.ext import ndb
from google.appengine.api import memcache
from app.models.Travel import IATA
from app.api.handler import ApiRequestHandler


class Airports(ApiRequestHandler):
    """ get the list of all airports, with details or not """

    def get(self):
        airports = memcache.get(self.memcache_key)
        if airports:
            return self.render_success({'airports': airports})

        details = self.request.get('details') != 'no'

        if details:
            airports = IATA.query().fetch(keys_only=False)
        else:
            airports = [iata.id() for iata in IATA.query().fetch(keys_only=True)]

        memcache.set(self.memcache_key, airports)
        return self.render_success({'airports': airports})


class Airport(ApiRequestHandler):
    """ get particular airport's details """

    def get(self, iata):
        return self.render_success(self.iata)


class Connections(ApiRequestHandler):
    """ get a particular airport's list of connections, detailed or in a simple list """

    def get(self, iata):
        details = self.request.get('details') != 'no'

        if details:
            airports = ndb.get_multi([ndb.Key('IATA', con) for con in self.iata.connections])
        else:
            airports = self.iata.connections

        return self.render_success({'airports': airports})
