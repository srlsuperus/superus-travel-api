# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from dateutil import parser
from google.appengine.ext import ndb, deferred

from app.api.handler import ApiRequestHandler
from app.models.Travel import Flight, FlightHistory, Route


class HistoricalLowest(ApiRequestHandler):
    """ returns a lowest fare ever known for a given route """

    def get(self, origin, destination):
        limit = int(self.request.get('limit', 1))
        route = ndb.Key('IATA', origin, 'IATA', destination)

        historical_price = FlightHistory.query(ancestor=route).order(FlightHistory.price).fetch(limit=limit)

        return self.render_success(historical_price) if historical_price else self.render_fail()


class Search(ApiRequestHandler):
    def get(self, origin, destination):
        oneway = self.request.get('oneway')

        flights_outbound = Flight.query(
            Flight.origin == origin,
            Flight.destination == destination,
            Flight.departure > datetime.datetime.now()
        ).fetch_async()

        flights_inbound = Flight.query(
            Flight.origin == destination,
            Flight.destination == origin,
            Flight.departure > datetime.datetime.now()
        ).fetch_async() if not oneway else None

        return self.render_success({
            "outbound": flights_outbound.get_result(),
            "inbound": flights_inbound.get_result() if flights_inbound else None
        })


class Discover(ApiRequestHandler):
    def get(self, iata, destination=None):
        with self.util.Timer('whole request'):

            limit = int(self.request.get('limit', 10))
            max_price = int(self.request.get('max_price', 1000))
            min_days_to_stay = int(self.request.get('min_days_to_stay', 3))
            max_days_to_stay = int(self.request.get('max_days_to_stay', 6))

            after = self.request.get('after', None)
            if after:
                after = parser.parse(after).replace(hour=0, minute=0, second=0)

            before = self.request.get('before', None)
            if before:
                before = parser.parse(before).replace(hour=23, minute=59, second=59)

            if after and before and after > before:
                return self.render_fail('The "after" date cannot go after the "before" date')

            now = datetime.datetime.now()

            if not destination:
                routes = Route.query(ancestor=self.iata.key).fetch(keys_only=True)
                destinations = [route.parent().id() for route in routes]
            else:
                destinations = [destination.strip().upper()]

            all_flights_in = {}
            all_flights_out = {}

            for destination in destinations:
                all_flights_out[destination] = Flight.query(
                    Flight.origin == iata,
                    Flight.destination == destination,
                    Flight.departure >= (after or now)
                ).fetch_async()

                if not before:
                    all_flights_in[destination] = Flight.query(
                        Flight.origin == destination,
                        Flight.destination == iata,
                        Flight.departure > (after or now)
                    ).fetch_async()
                else:
                    all_flights_in[destination] = Flight.query(
                        Flight.origin == destination,
                        Flight.destination == iata,
                        Flight.departure > (after or now),
                        Flight.departure <= before
                    ).fetch_async()

            results = {}

            with self.util.Timer('main logic'):
                for destination, flights_out in all_flights_out.iteritems():

                    results[destination] = []

                    for flight_out in flights_out.get_result():

                        for flight_in in all_flights_in[destination].get_result():
                            diff_in_days = (flight_in.departure - flight_out.departure).days
                            if min_days_to_stay <= diff_in_days <= max_days_to_stay:

                                cost = flight_out.price + flight_in.price
                                if cost <= max_price:
                                    results[destination].append({
                                        'days': diff_in_days,
                                        'currency': flight_out.currency,
                                        'cost': float('{0:.2f}'.format(cost)),
                                        'flight_out': {
                                            'price': flight_out.price,
                                            'currency': flight_out.currency,
                                            'time': str(flight_out.departure),
                                            'as_of': str(flight_out.updated)
                                        },
                                        'flight_in': {
                                            'price': flight_in.price,
                                            'currency': flight_in.currency,
                                            'time': str(flight_in.departure),
                                            'as_of': str(flight_in.updated)
                                        }
                                    })

                    results[destination] = sorted(results[destination], key=lambda k: k['cost'])[:limit]

            return self.render_success(results)


class FlightPriceGraph(ApiRequestHandler):
    def get(self, origin, destination, depart):

        try:
            depart = parser.parse(depart).date()
        except ValueError as v:
            return self.render_fail('Value error exception while parsing {}: {}'.format(depart, str(v)))

        flight_key = ndb.Key('IATA', origin, 'IATA', destination, 'Flight', int(depart.strftime('%Y%m%d')))
        return self.render_success(
            sorted(FlightHistory.query(ancestor=flight_key).fetch(), key=lambda x: x.reported, reverse=True))


def delete_keys(keys):
    ndb.delete_multi(keys)


class CleanUpDatabase(ApiRequestHandler):
    """ a handler for cleaning up old flight data """

    def get(self):
        delay = 0

        three_months_ago = datetime.datetime.now() - datetime.timedelta(days=3 * 28)
        cursor = None
        while True:
            keys, next_cursor, more = \
                Flight.query(Flight.departure <= three_months_ago).fetch_page(1000, keys_only=True, start_cursor=cursor)
            if keys:
                deferred.defer(delete_keys, keys, _countdown=delay)
                delay += 5
                self.log.info("Scheduled {} Flight entities for deletion".format(len(keys)))
            if more and next_cursor:
                cursor = next_cursor
            else:
                break

        twelve_months_ago = datetime.datetime.now() - datetime.timedelta(days=365)
        cursor = None
        while True:
            keys, next_cursor, more = \
                FlightHistory.query(FlightHistory.reported <= twelve_months_ago).fetch_page(1000, keys_only=True,
                                                                                            start_cursor=cursor)
            if keys:
                deferred.defer(delete_keys, keys, _countdown=delay)
                delay += 5
                self.log.info("Scheduled {} FlightHistory entities for deletion".format(len(keys)))
            if more and next_cursor:
                cursor = next_cursor
            else:
                break
