# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
import os
from calendar import monthrange

from dateutil import rrule
from google.appengine.api import taskqueue, urlfetch
from google.appengine.ext import deferred, ndb

from app.api.handler import ApiRequestHandler
from app.models.Travel import Airline, IATA, Flight, FlightHistory, Route
from app.utils import Util

API_METADATA = 'https://wizzair.com/static/metadata.json'
API_URL_OLD = 'https://be.wizzair.com/7.9.2/Api'
AIRLINE_IATA_CODE = 'W6'
AIRPORTS_TO_WATCH = os.environ.get('AIRPORTS_TO_WATCH').split(' ')
RETRIES = 3


class Airports(ApiRequestHandler):
    """ Get available airports / cities and their destinations """

    def get(self):
        API_URL = Airline.get_or_insert(AIRLINE_IATA_CODE, api_url=API_URL_OLD).api_url
        url = "{}{}".format(API_URL, "/asset/map?languageCode=en-gb")

        for i in xrange(RETRIES):
            try:
                res = urlfetch.fetch(url, deadline=20)
                if res.status_code != 200:
                    message = 'Code {} when fetching "{}". Content = {}'.format(res.status_code, url, res.content)
                    self.log.warning(message)
                    continue
                break
            except (urlfetch.ConnectionClosedError, urlfetch.DeadlineExceededError) as e:
                self.log.warning("Raised {} ({}) when fetching {}".format(e.__class__.__name__, str(e), url))
                continue
        else:
            message = "Exhausted {} attempts while requesting {}".format(RETRIES, url)
            self.log.error(message)
            return self.render_fail(message)

        res = self.json.decode(res.content)

        tasks = []

        deferred.defer(store_routes_helper, cities=res['cities'])
        deferred.defer(store_airports_helper, cities=res['cities'])

        tasks_log = []
        for watching_airport in AIRPORTS_TO_WATCH:
            for airport in res['cities']:
                if airport['iata'] == watching_airport:
                    for connection in airport['connections']:
                        tasks.append(
                            taskqueue.Task(
                                url=self.uri_for('wizzair-timetable'),
                                params={
                                    'origin': airport['iata'],
                                    'destination': connection['iata']
                                })
                        )
                        tasks_log.append('{} -> {}'.format(airport['iata'], connection['iata']))

        queue_travel = taskqueue.Queue('travel')
        for batch in Util.batch(tasks, 100):
            queue_travel.add(batch)
        message = 'Queued the following routes: {}'.format(", ".join(tasks_log))
        self.log.info(message)
        return self.render_success(message)


def store_airports_helper(cities):
    """ helper that loops & saves all new / changed airport codes """

    entities_to_store = []
    for batch in Util.batch(cities, 25):
        iatas = ndb.get_multi([ndb.Key('IATA', city['iata']) for city in batch])
        for i, iata_entity in enumerate(iatas):

            iata = batch[i]['iata']
            aliases = batch[i]['aliases']
            shortName = batch[i]['shortName']

            try:
                aliases.remove(shortName)
            except ValueError:
                pass

            connections = [connection['iata'] for connection in batch[i]['connections']]

            if not iata_entity:
                iata_entity = IATA(
                    id=iata,
                    iata=iata,
                    shortName=shortName,
                    aliases=aliases,
                    countryName=batch[i]['countryName'],
                    countryCode=batch[i]['countryCode'],
                    coordinates=ndb.GeoPt(lat=batch[i]['latitude'], lon=batch[i]['longitude']),
                    connections=connections
                )
                entities_to_store.append(iata_entity)
                logging.warn('AWESOME! New {} IATA info has been created!'.format(iata))
            elif iata_entity.connections != connections or iata_entity.aliases != aliases:
                iata_entity.aliases = aliases
                iata_entity.connections = connections
                entities_to_store.append(iata_entity)
                logging.warn('Cool! {} IATA info has been updated!'.format(iata))

    if len(entities_to_store) > 0:
        for batch in Util.batch(entities_to_store, 50):
            ndb.put_multi(batch)
        logging.info('{} airport entities were updated'.format(len(entities_to_store)))
    else:
        logging.debug('No airport data were updated...')


def store_routes_helper(cities):
    """ helper that loops & saves all new / changed airline routes """

    entities_to_store = []
    for city in cities:
        origin = city['iata']
        connections = city['connections']

        routes = [ndb.Key('IATA', origin, 'IATA', con['iata'], 'Route', AIRLINE_IATA_CODE) for con in connections]

        for i, route in enumerate(ndb.get_multi(routes)):
            if not route:
                route = Route(
                    id=AIRLINE_IATA_CODE,
                    parent=ndb.Key('IATA', origin, 'IATA', connections[i]['iata']),
                    watch=origin in AIRPORTS_TO_WATCH or connections[i]['iata'] in AIRPORTS_TO_WATCH
                )
                entities_to_store.append(route)
            elif not route.watch and (origin in AIRPORTS_TO_WATCH or connections[i]['iata'] in AIRPORTS_TO_WATCH):
                route.watch = True
                entities_to_store.append(route)

    if len(entities_to_store) > 0:
        for batch in Util.batch(entities_to_store, 50):
            ndb.put_multi(batch)
        logging.info('{} airline routes were created'.format(len(entities_to_store)))
    else:
        logging.debug('No new airline routes were created...')


class Timetable(ApiRequestHandler):
    """ Get a timetable for flights from (origin) -> to (destination) airports / cities """

    def post(self):
        origin = self.request.get('origin')
        destination = self.request.get('destination')

        departure_station = origin
        arrival_station = destination

        if not origin or not destination or len(origin) != 3 or len(destination) != 3:
            return self.render_fail("Both origin & destination parameters are required with valid values")

        API_URL = Airline.get_or_insert(AIRLINE_IATA_CODE, api_url=API_URL_OLD).api_url
        url = "{}{}".format(API_URL, "/search/timetable")

        now = datetime.datetime.now()
        for dt in rrule.rrule(rrule.MONTHLY, dtstart=datetime.datetime.now(), count=12):

            self.log.debug('Searching for {} <-> {} flights in {}'.format(origin, destination, dt.strftime('%B, %Y')))

            date_start = now.strftime('%Y-%m-%d') if dt < now else dt.strftime('%Y-%m-01')
            date_end = dt.strftime('%Y-%m-{}').format(monthrange(dt.year, dt.month)[1])

            headers = {
                'Content-Type': 'application/json',
                'Origin': 'https://wizzair.com',
                'Referer': 'https://wizzair.com/en-gb/flights/timetable/{}/{}'.format(departure_station, arrival_station),
                'Accept': 'application/json, text/plain, */*'
            }

            payload = self.json.encode({
                "adultCount": 1,
                "childCount": 0,
                "infantCount": 0,
                "priceType": "wdc",
                "flightList": [
                    {"departureStation": origin, "arrivalStation": destination, "from": date_start, "to": date_end},
                    {"departureStation": destination, "arrivalStation": origin, "from": date_start, "to": date_end}
                ]
            })

            for i in xrange(RETRIES):
                try:
                    res = urlfetch.fetch(url, method=urlfetch.POST, deadline=20, payload=payload, headers=headers)
                    if res.status_code != 200:
                        message = 'Code {} when fetching "{}". Content = {}'.format(res.status_code, url, res.content)
                        self.log.warning(message)
                        continue
                    break
                except (urlfetch.ConnectionClosedError, urlfetch.DeadlineExceededError) as e:
                    self.log.warning("Exception {} ({}) when fetching {}".format(e.__class__.__name__, str(e), url))
                    continue
            else:
                self.log.error("Exhausted {} attempted when requesting {}".format(RETRIES, url))
                continue

            res = self.json.decode(res.content)

            entities_to_store = []
            flights = (res.get('outboundFlights') or []) + (res.get('returnFlights') or [])
            for flight_info in flights:
                if flight_info['priceType'] == 'price':

                    departure_station = flight_info['departureStation']
                    arrival_station = flight_info['arrivalStation']
                    flight_route = ndb.Key('IATA', departure_station, 'IATA', arrival_station)

                    departure_date = datetime.datetime.strptime(flight_info['departureDate'], "%Y-%m-%dT%H:%M:%S")
                    departure_time = datetime.datetime.strptime(flight_info['departureDates'][0], "%Y-%m-%dT%H:%M:%S")

                    flight_currency = flight_info['price']['currencyCode']
                    flight_price = float(flight_info['price']['amount'])
                    flight_id = int(departure_date.strftime('%Y%m%d'))
                    flight = Flight.get_by_id(flight_id, flight_route)

                    historical_price = FlightHistory(
                        parent=ndb.Key('IATA', departure_station, 'IATA', arrival_station, 'Flight', flight_id),
                        currency=flight_currency,
                        price=flight_price,
                        departure=departure_time
                    )

                    if not flight:
                        flight = Flight(
                            id=flight_id,
                            parent=flight_route,
                            origin=departure_station,
                            destination=arrival_station,
                            airline=AIRLINE_IATA_CODE,
                            price=flight_price,
                            currency=flight_currency,
                            departure=departure_time,
                        )
                        entities_to_store.append(flight)
                        entities_to_store.append(historical_price)
                        logging.debug(
                            'New {}->{} flight on {} added for {} {}!'.format(
                                departure_station, arrival_station, flight_id, flight_price, flight_currency
                            )
                        )
                    elif flight.price != flight_price or flight_currency != flight_currency:
                        change = int((abs(flight.price - flight_price) / ((flight.price + flight_price) / 2)) * 100)
                        logging.debug(
                            'Flight {dep}->{arr} on {flight_id} {update} by {change} '
                            '(from {price_old} to {price_new} {price_currency})'.format(
                                dep=departure_station,
                                arr=arrival_station,
                                flight_id=flight_id,
                                update="increased" if flight.price < flight_price else "decreased",
                                change="{}%".format(change),
                                price_old=flight.price,
                                price_new=flight_price,
                                price_currency=flight_currency
                            )
                        )
                        flight.price = flight_price
                        flight.currency = flight_currency
                        entities_to_store.append(flight)
                        entities_to_store.append(historical_price)

            if len(entities_to_store) > 0:
                for batch in Util.batch(entities_to_store, 50):
                    ndb.put_multi(batch)
                self.log.debug('{} flights found, {} created / updated'.format(len(flights), len(entities_to_store)))
            else:
                self.log.debug('{} flights found, none created / updated'.format(len(flights)))


class ApiUrlRefresh(ApiRequestHandler):
    def get(self):
        airline = Airline.get_or_insert_async(AIRLINE_IATA_CODE, name="WizzAir", api_url=None)
        for i in xrange(RETRIES):
            try:
                res = urlfetch.fetch(API_METADATA, deadline=20)
                if res.status_code != 200:
                    msg = 'Code {} when fetching "{}". Content = {}'.format(res.status_code, API_METADATA, res.content)
                    self.log.warning(msg)
                    continue
                break
            except (urlfetch.ConnectionClosedError, urlfetch.DeadlineExceededError) as e:
                self.log.warning("Raised {} ({}) when fetching {}".format(e.__class__.__name__, str(e), API_METADATA))
                continue
        else:
            message = "Could not refresh the API token"
            self.log.error(message)
            return self.render_fail(message)

        res = self.json.decode(res.content)
        API_URL = res['apiUrl']

        airline = airline.get_result()
        if airline.api_url != API_URL:
            airline.api_url = API_URL
            airline.put()
            self.log.info("The new API URL is {}".format(airline.api_url))
        else:
            self.log.debug("API URL remained unchanged as {}".format(airline.api_url))

        return self.render_success(airline.api_url)
