# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from google.appengine.ext import ndb
from . import utils


class IATA(utils.TrackableModel):
    """ Contains airport information """

    # id = airport code / iata
    iata = ndb.StringProperty(required=True, indexed=False)
    shortName = ndb.StringProperty(required=True, indexed=False)
    aliases = ndb.StringProperty(repeated=True, indexed=False)
    countryName = ndb.StringProperty(required=True, indexed=False)
    countryCode = ndb.StringProperty(required=True, indexed=False)
    coordinates = ndb.GeoPtProperty(required=True, indexed=True)
    connections = ndb.StringProperty(repeated=True, indexed=True)  # list of refs to itself


class Airline(utils.TrackableModel):
    """ Airline info """

    # id = IATA code
    name = ndb.StringProperty(indexed=False)
    api_url = ndb.StringProperty(indexed=False)


class Route(utils.TrackableModel):
    """ Contains list of all routes with IDs being an Airline reference  """

    # parent = ndb.Key('IATA', origin, 'IATA', destination)
    # id = Airline IATA code
    watch = ndb.BooleanProperty(default=False, indexed=True)


class Flight(utils.TrackableModel):
    """ Contains flight information """

    # id = departure date
    origin = ndb.StringProperty(required=True, indexed=True)  # a ref to IATA model
    destination = ndb.StringProperty(required=True, indexed=True)  # a ref to IATA model
    airline = ndb.StringProperty(default='W6', indexed=False)  # a ref to an Airline model
    price = ndb.FloatProperty(required=True, indexed=True)
    currency = ndb.StringProperty(required=True, indexed=True)
    departure = ndb.DateTimeProperty(required=True, indexed=True)


class FlightHistory(utils.CustomModel):
    """ Tracking the price changes of a flight """

    # parent = Flight id
    price = ndb.FloatProperty(required=True, indexed=True)
    currency = ndb.StringProperty(required=True, indexed=False)
    departure = ndb.DateTimeProperty(required=True, indexed=False)
    reported = ndb.DateTimeProperty(required=True, auto_now_add=True, indexed=True)
