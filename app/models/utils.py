# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from google.appengine.ext import ndb
import logging
from app.utils import Util


class CustomModel(ndb.Model):
    """ Custom NDB Model, to be inherited by all models """

    to_json = Util.to_json

    @classmethod
    def query_page(cls, size=10, bookmark=None, is_prev=None, equality_filters=None, orders=None, ancestor_key=None,
                   q=None, keys_only=None):
        """
            Generate a paginated result of current class
                Param cls: Current ndb model class to query
                Param size: The size of the results
                Param bookmark: The urlsafe cursor of the previous queries. First time will be None
                Param is_prev: If your requesting for a next result or the previous ones
                Param equal_filters: a dictionary of {'property': value} to apply equality filters only
                Param orders: a dictionary of {'property': '-' or ''} to order the results like .order(cls.property)
                Param ancestor_key: they key of ancestor if any
                Return: a tuple (list of results, Previous cursor bookmark, Next cursor bookmark)
        """

        if bookmark:
            cursor = ndb.Cursor(urlsafe=bookmark)
        else:
            is_prev = None
            cursor = None

        q = q or (cls.query(ancestor=ancestor_key) if ancestor_key else cls.query())
        try:
            if equality_filters:
                for prop, value in equality_filters.iteritems():
                    q = q.filter(getattr(cls, prop) == value)

            q_forward = q.filter()
            q_reverse = q.filter()

            if orders:
                for prop, value in orders.iteritems():
                    if value == '-':
                        q_forward = q_forward.order(-getattr(cls, prop))
                        q_reverse = q_reverse.order(getattr(cls, prop))
                    else:
                        q_forward = q_forward.order(getattr(cls, prop))
                        q_reverse = q_reverse.order(-getattr(cls, prop))
        except Exception as e:
            logging.warn('Query Page Exception: {}'.format(e))
            return {'results': None, 'prev_bookmark': None, 'next_bookmark': None}
        if is_prev:
            qry = q_reverse
            new_cursor = cursor.reversed() if cursor else None
        else:
            qry = q_forward
            new_cursor = cursor if cursor else None

        results, new_cursor, more = qry.fetch_page(size or None, start_cursor=new_cursor, keys_only=keys_only)
        if more and new_cursor:
            more = True
        else:
            more = False

        if is_prev:
            prev_bookmark = new_cursor.reversed().urlsafe() if more else None
            next_bookmark = bookmark
            results.reverse()
        else:
            prev_bookmark = bookmark
            next_bookmark = new_cursor.urlsafe() if more else None

        return {'results': results, 'prev_bookmark': prev_bookmark, 'next_bookmark': next_bookmark}


class TrackableModel(CustomModel):
    """ simple way of adding created / updated properties to each ndb.Model """

    updated = ndb.DateTimeProperty(auto_now=True, indexed=False)
    created = ndb.DateTimeProperty(auto_now_add=True, indexed=False)
