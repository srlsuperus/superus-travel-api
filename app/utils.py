# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
import time

from google.appengine.ext import ndb


class Util:
    def __init__(self):
        pass

    @staticmethod
    def batch(iterable, n=1):
        """
        divide a list into batches of N

        for x in batch(range(0, 10), 3):
            print x

        will print:
            [0, 1, 2]
            [3, 4, 5]
            [6, 7, 8]
            [9]
        """

        l = len(iterable)
        for ndx in range(0, l, n):
            yield iterable[ndx:min(ndx + n, l)]

    @staticmethod
    def to_json(obj):
        """ render an object (mostly ndb) to json """

        if isinstance(obj, list):
            return [Util.to_json(l) for l in obj]

        if isinstance(obj, dict):
            x = {}
            for l in obj:
                x[l] = Util.to_json(obj[l])
            return x

        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()

        if isinstance(obj, ndb.GeoPt):
            return {
                'lat': obj.lat,
                'lon': obj.lon
            }

        if isinstance(obj, ndb.Key):
            return {
                'id': obj.id(),
                'urlsafe': obj.urlsafe(),
                'parent': Util.to_json(obj.parent())
            }

        if isinstance(obj, ndb.Model):
            dct = obj.to_dict()
            dct['key'] = Util.to_json(obj.key)
            return Util.to_json(dct)

        return obj

    class Timer:
        def __init__(self, msg=None):
            self.msg = msg

        def __enter__(self):
            self.start = time.clock()
            return self

        def __exit__(self, *args):
            self.end = time.clock()
            self.interval = self.end - self.start
            logging.info("{} took %.03f sec.".format(self.msg) % self.interval)
