import os

from google.appengine.ext import vendor

SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE', 'Development')
PRODUCTION_MODE = not SERVER_SOFTWARE.startswith('Development')
TEST_MODE = not PRODUCTION_MODE and 'testbed' in SERVER_SOFTWARE

if os.path.isdir('libs'):
    vendor.add('libs')
elif os.path.isdir('../libs'):
    vendor.add('../libs')

appstats_SHELL_OK = not PRODUCTION_MODE
