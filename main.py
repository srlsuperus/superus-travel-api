# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from webapp2 import WSGIApplication, Route
from webapp2_extras.routes import PathPrefixRoute, NamePrefixRoute

from app import api

# a hack to allow webapp2 to accept PATCH requests
WSGIApplication.allowed_methods = WSGIApplication.allowed_methods.union(('PATCH'))

app = WSGIApplication([

    PathPrefixRoute('/wizzair', [NamePrefixRoute('wizzair-', [  # wizzair-
        Route('/airports', api.travel.wizzair.Airports, 'airports'),
        Route('/timetable', api.travel.wizzair.Timetable, 'timetable'),
        Route('/api_url_refresh', api.travel.wizzair.ApiUrlRefresh, 'api-url-refresh'),
    ])]),

    PathPrefixRoute('/search', [NamePrefixRoute('search-', [  # search

        Route('/discover/<iata>', api.travel.helpers.Discover, 'discover'),
        Route('/discover/<iata>/<destination>', api.travel.helpers.Discover, 'discover'),
        Route('/<origin>/<destination>', api.travel.helpers.Search, 'search'),

        PathPrefixRoute('/historical', [NamePrefixRoute('historical-', [  # search-historical
            Route('/lowest/<origin>/<destination>', api.travel.helpers.HistoricalLowest, 'lowest')
        ])]),

    ])]),

    Route('/graph/<origin>/<destination>/<depart>', api.travel.helpers.FlightPriceGraph, 'graph'),

    PathPrefixRoute('/sitemap', [NamePrefixRoute('sitemap-', [  # sitemap -
        Route('/destinations', api.sitemap.Destinations, 'destinations'),
        Route('/flights/<origin>/<destination>', api.sitemap.Flights, 'flights')
    ])]),

    Route('/airports', api.travel.airports.Airports, 'airports'),  # airports
    PathPrefixRoute('/airports', [
        Route('/<iata>', api.travel.airports.Airport, 'airport'),  # airport
        PathPrefixRoute('/<iata>', [
            Route('/connections', api.travel.airports.Connections, 'connections')
        ])
    ]),

    PathPrefixRoute('/helpers', [
        PathPrefixRoute('/cron', [
            Route('/cleanup_db', api.travel.helpers.CleanUpDatabase)
        ])
    ]),

], debug=False)
