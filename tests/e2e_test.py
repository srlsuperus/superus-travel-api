# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import requests

HOST_NAME = os.getenv('CLOUDSDK_CORE_PROJECT', 'superus-travel')
HOST = "https://api-dot-{hostName}.appspot.com".format(hostName=HOST_NAME)

# [START e2e]
response = requests.get("{host}/".format(host=HOST))
html = response.content
assert('<h1>404 Not Found</h1>' in html)
# [END e2e]
