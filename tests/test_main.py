# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from main import app
from test_setup import TestClient


class HomePageTest(TestClient):
    def test_homepage_is_404(self):
        """ make sure the "/" route responds with 404 and the text supporting it"""

        res = app.get_response('/')
        self.assertEqual(res.status_int, 404)
        self.assertIn('404 Not Found', res.body)


if __name__ == '__main__':
    TestClient.unittest.main()
