# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from google.appengine.ext import testbed


class TestClient(unittest.TestCase):
    unittest = unittest

    def setUp(self):
        self.testbed = testbed.Testbed()  # first, create an instance of the Testbed class.
        self.testbed.activate()  # then activate the testbed, which will allow you to use service stubs.

        # declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        # Don't forget to deactivate the testbed after the tests are completed.
        # If the testbed is not deactivated, the original stubs will not be restored.
        self.testbed.deactivate()
