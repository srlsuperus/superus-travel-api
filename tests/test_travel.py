# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from google.appengine.ext import ndb
from webapp2_extras import json

from app.models.Travel import FlightHistory
from main import app
from test_setup import TestClient


class ExternalTravelLowestAPITest(TestClient):
    """ test the "/search/historical/lowest/<origin>/<destination>" route """

    URL = '/search/historical/lowest/{}/{}'

    def test_invalid_locations(self):
        """ test the route with invalid origin / destination locations """

        origin = 'XYZ'
        destination = 'XYZ'

        res = app.get_response(self.URL.format(origin, destination))
        self.assertEqual(res.status_int, 403)

        data = json.decode(res.body)
        self.assertIsInstance(data, dict)
        self.assertEqual(data["status"], "fail")

    def test_valid_locations(self):
        """ test the route with valid origin / destination locations """

        origin = 'SFO'
        destination = 'JFK'
        route = ndb.Key('IATA', origin, 'IATA', destination)

        flight_history = FlightHistory(parent=route, price=0.99, currency="USD", departure=datetime.now())
        flight_history_key = flight_history.put()
        self.assertIsNotNone(flight_history_key)

        res = app.get_response(self.URL.format(origin, destination))
        self.assertEqual(res.status_int, 200)
        data = json.decode(res.body)
        desired = flight_history.to_json()

        self.assertIsInstance(data, dict)
        self.assertEqual(data["status"], "success")
        self.assertIsInstance(data["data"], list)
        self.assertEqual(len(data["data"]), 1)
        self.assertDictEqual(data["data"][0], desired)


if __name__ == '__main__':
    TestClient.unittest.main()
